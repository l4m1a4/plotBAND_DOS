# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 10:04:41 2018

@author: phamtronglam2001@gmail.com
adopted and modified from
http://home.ustc.edu.cn/~lipai/scripts/vasp_scripts/python_plot_dos_band.html
https://github.com/QijingZheng/pyband #for converting wavevector character to Greek symbol
Also modification of plotter module in pymatgen.electronic_structure were done to plot to axis handle
instead of creating completely new plot

Requirement:
    Band calculation: vasprun.xml, KPOINTS
    DOS calculation: vasprun.xml, POTCAR
"""
from plotBAND_DOS import plotBAND_DOS_2Dmaterial_style, plotBAND_DOS_pretty_style


"""
Silicene
"""
vb_energy_range=3
cb_energy_range=1


plotBAND_DOS_pretty_style(dosxml='/mnt/d/iop.dvan/data/vvon/SiSVB_acetone/DOS/vasprun.xml',
                              dosPOTCAR='/mnt/d/iop.dvan/data/vvon/SiSVB_acetone/DOS/POTCAR',
                              bandxml='/mnt/d/iop.dvan/data/vvon/SiSVB_acetone/BAND/vasprun.xml',
                              bandKPT='/mnt/d/iop.dvan/data/vvon/SiSVB_acetone/BAND/KPOINTS',
                              imagename='SiSVB_acetone.png',
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=False,
                              system='SiSVB_acetone')


plotBAND_DOS_2Dmaterial_style(dosxml='/mnt/d/iop.dvan/data/vvon/SiSVB_acetone/DOS/vasprun.xml',
                              dosPOTCAR='/mnt/d/iop.dvan/data/vvon/SiSVB_acetone/DOS/POTCAR',
                              bandxml='/mnt/d/iop.dvan/data/vvon/SiSVB_acetone/BAND/vasprun.xml',
                              bandKPT='/mnt/d/iop.dvan/data/vvon/SiSVB_acetone/BAND/KPOINTS',
                              imagename='SiSVB_acetone.png',
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=False,
                              system='SiSVB_acetone',
                              inset=False)

