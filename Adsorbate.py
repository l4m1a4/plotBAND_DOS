# -*- coding: utf-8 -*-
"""
Created on Tue Oct 23 07:22:39 2018

@author: Administrator
"""

from pymatgen.io.vasp import Vasprun
from pymatgen.electronic_structure.plotter import DosPlotter

xlim = [-6,4]
ylim = [0,20]
ylimD = [0,40]
zero_at_efermi=False
sigma = None

v = Vasprun('../Silicene_pristine_4x4/2-optPBE/K12x12/DOS/vasprun.xml')
cdos = v.complete_dos
element_dos = cdos.get_element_dos()
plotter = DosPlotter(zero_at_efermi=zero_at_efermi,sigma=sigma)
plotter.add_dos_dict(element_dos)
#plotter.add_dos('Total DOS', cdos)
plt=plotter.get_plot(xlim=xlim, ylim=ylim)
plt.title('Silicene 4x4')
plt.savefig('Silicene4x4.png')


v = Vasprun('../Si_acetone/dft-d3/K6x6/DOS_adsorbate/vasprun.xml')
cdos = v.complete_dos
element_dos = cdos.get_element_dos()
plotter = DosPlotter(zero_at_efermi=zero_at_efermi,sigma=sigma)
plotter.add_dos_dict(element_dos)
#plotter.add_dos('Total DOS', cdos)
plt=plotter.get_plot(xlim=xlim, ylim=ylim)
plt.title('Acetone')
plt.savefig('Acetone.png')
v = Vasprun('../Si_acetone/dft-d3/K12x12/DOS/vasprun.xml')
cdos = v.complete_dos
element_dos = cdos.get_element_dos()
plotter = DosPlotter(zero_at_efermi=zero_at_efermi,sigma=sigma)
plotter.add_dos_dict(element_dos)
#plotter.add_dos('Total DOS', cdos)
plt=plotter.get_plot(xlim=xlim, ylim=ylimD)
plt.title('Acetone-Si')
plt.ylim(ylimD)
plt.savefig('Acetone-Si.png')


exit

v = Vasprun('../Si_benzothiazole/dft-d3/K6x6/DOS_adsorbate/vasprun.xml')
cdos = v.complete_dos
element_dos = cdos.get_element_dos()
plotter = DosPlotter(zero_at_efermi=zero_at_efermi,sigma=sigma)
plotter.add_dos_dict(element_dos)
#plotter.add_dos('Total DOS', cdos)
plt=plotter.get_plot(xlim=xlim, ylim=ylim)
plt.title('Benzothiazole')
plt.savefig('Benzothiazole.png')
v = Vasprun('../Si_benzothiazole/dft-d3/K12x12/DOS/vasprun.xml')
cdos = v.complete_dos
element_dos = cdos.get_element_dos()
plotter = DosPlotter(zero_at_efermi=zero_at_efermi,sigma=sigma)
plotter.add_dos_dict(element_dos)
#plotter.add_dos('Total DOS', cdos)
plt=plotter.get_plot(xlim=xlim, ylim=ylimD)
plt.title('Benzothiazole-Si')
plt.ylim(ylimD)
plt.savefig('Benzothiazole-Si.png')




v = Vasprun('../Si_isopropanol/dft-d3/K6x6/DOS_adsorbate/vasprun.xml')
cdos = v.complete_dos
element_dos = cdos.get_element_dos()
plotter = DosPlotter(zero_at_efermi=zero_at_efermi,sigma=sigma)
plotter.add_dos_dict(element_dos)
#plotter.add_dos('Total DOS', cdos)
plt=plotter.get_plot(xlim=xlim, ylim=ylim)
plt.title('isopropanol')
plt.savefig('isopropanol.png')
v = Vasprun('../Si_isopropanol/dft-d3/K12x12/DOS/vasprun.xml')
cdos = v.complete_dos
element_dos = cdos.get_element_dos()
plotter = DosPlotter(zero_at_efermi=zero_at_efermi,sigma=sigma)
plotter.add_dos_dict(element_dos)
#plotter.add_dos('Total DOS', cdos)
plt=plotter.get_plot(xlim=xlim, ylim=ylimD)
plt.title('isopropanol-Si')
plt.ylim(ylimD)
plt.savefig('isopropanol-Si.png')





v = Vasprun('../Si_toluene/dft-d3/K6x6/DOS_adsorbate/vasprun.xml')
cdos = v.complete_dos
element_dos = cdos.get_element_dos()
plotter = DosPlotter(zero_at_efermi=zero_at_efermi,sigma=sigma)
plotter.add_dos_dict(element_dos)
#plotter.add_dos('Total DOS', cdos)
plt=plotter.get_plot(xlim=xlim, ylim=ylim)
plt.title('toluene')
plt.savefig('toluene.png')
v = Vasprun('../Si_toluene/dft-d3/K12x12/DOS/vasprun.xml')
cdos = v.complete_dos
element_dos = cdos.get_element_dos()
plotter = DosPlotter(zero_at_efermi=zero_at_efermi,sigma=sigma)
plotter.add_dos_dict(element_dos)
#plotter.add_dos('Total DOS', cdos)
plt=plotter.get_plot(xlim=xlim, ylim=ylimD)
plt.title('toluene-Si')
plt.ylim(ylimD)
plt.savefig('toluene-Si.png')



v = Vasprun('../Si_trichloroethene/dft-d3/K6x6/DOS_adsorbate/vasprun.xml')
cdos = v.complete_dos
element_dos = cdos.get_element_dos()
plotter = DosPlotter(zero_at_efermi=zero_at_efermi,sigma=sigma)
plotter.add_dos_dict(element_dos)
#plotter.add_dos('Total DOS', cdos)
plt=plotter.get_plot(xlim=xlim, ylim=ylim)
plt.title('trichloroethene')
plt.savefig('trichloroethene.png')
v = Vasprun('../Si_trichloroethene/dft-d3/K12x12/DOS/vasprun.xml')
cdos = v.complete_dos
element_dos = cdos.get_element_dos()
plotter = DosPlotter(zero_at_efermi=zero_at_efermi,sigma=sigma)
plotter.add_dos_dict(element_dos)
#plotter.add_dos('Total DOS', cdos)
plt=plotter.get_plot(xlim=xlim, ylim=ylimD)
plt.title('trichloroethene-Si')
plt.ylim(ylimD)
plt.savefig('trichloroethene-Si.png')



