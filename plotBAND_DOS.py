# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 10:04:41 2018

@author: phamtronglam2001@gmail.com
adopted and modified from
http://home.ustc.edu.cn/~lipai/scripts/vasp_scripts/python_plot_dos_band.html
https://github.com/QijingZheng/pyband #for converting wavevector character to Greek symbol
Also modification of plotter module in pymatgen.electronic_structure were done to plot to axis handle
instead of creating completely new plot

Requirement:
    Band calculation: vasprun.xml, KPOINTS
    DOS calculation: vasprun.xml, POTCAR
"""
import matplotlib as mpl
from pymatgen.io.vasp import Vasprun, BSVasprun
from pymatgen.electronic_structure.plotter import DosPlotter, BSPlotter, BSDOSPlotter
from plotter_test import *

inset=True
def plotBAND_DOS_2Dmaterial_style(dosxml='vasprun_dos.xml',dosPOTCAR='POTCAR',
                                  bandxml='vasprun_band.xml',bandKPT='KPOINTS',
                                  imagename='demo.eps',
                                  dos_axis_scale=[1.05,1.05,1.05],
                                  vb_energy_range=6.0,
                                  cb_energy_range=1.5,
                                  title=False,
                                  system = 'VOC on Silicene',
                                  inset = False):
    dosdata = Vasprun(dosxml)
    tdos = dosdata.tdos
    cdos = dosdata.complete_dos
    element_dos = cdos.get_element_dos()

    band_data = BSVasprun(bandxml)
    band_struct = band_data.get_band_structure(kpoints_filename=bandKPT,line_mode=True)
    if inset==True:
        BandDos = BSDOSPlotter_2Dmaterial_style_inset(dos_axis_scale=dos_axis_scale,
                                            vb_energy_range=vb_energy_range,
                                            cb_energy_range=cb_energy_range)
        title=False

    if inset==False:
        BandDos = BSDOSPlotter_2Dmaterial_style(dos_axis_scale=dos_axis_scale,
                                            vb_energy_range=vb_energy_range,
                                            cb_energy_range=cb_energy_range)

    mplt=BandDos.get_plot(band_struct,cdos)
    if title==True:
        mplt.title(system + '\nBand gap: {:.0f} meV'.format(dosdata.eigenvalue_band_properties[0]*1000),loc='right')
    mplt.savefig(imagename,dpi=600)
    print(system, ': ', dosdata.eigenvalue_band_properties)


def plotBAND_DOS_pretty_style(dosxml='vasprun_dos.xml',dosPOTCAR='POTCAR',
                              bandxml='vasprun_band.xml',bandKPT='KPOINTS',
                              imagename='demo.eps',
                              vb_energy_range=6.0,
                              cb_energy_range=1.5,
                              title=False,
                              system = 'VOC on Silicene'):
    dosdata = Vasprun(dosxml)
    tdos = dosdata.tdos
    cdos = dosdata.complete_dos
    element_dos = cdos.get_element_dos()

    band_data = BSVasprun(bandxml)
    band_struct = band_data.get_band_structure(kpoints_filename=bandKPT,line_mode=True)
    BandDos = BSDOSPlotter_pretty(vb_energy_range=vb_energy_range,
                                  cb_energy_range=cb_energy_range,
				  bs_legend=None)
    mplt = BandDos.get_plot(band_struct,cdos)
    if title==True:
        mplt.title(system + '\nBand gap: {:.0f} meV'.format(dosdata.eigenvalue_band_properties[0]*1000))
    mplt.savefig(imagename,dpi=600)
    print(system, ': ',dosdata.eigenvalue_band_properties)


def plotBAND_DOS_molecule(dosxml='vasprun_dos.xml',dosPOTCAR='POTCAR',
                              bandxml='vasprun_band.xml',bandKPT='KPOINTS',
                              imagename='demo.eps',
                              vb_energy_range=6.0,
                              cb_energy_range=1.5,
                              title=False,
                              system = 'VOC on Silicene'):
    dosdata = Vasprun(dosxml)
    tdos = dosdata.tdos
    cdos = dosdata.complete_dos
    element_dos = cdos.get_element_dos()

    band_data = BSVasprun(bandxml)
    band_struct = band_data.get_band_structure(kpoints_filename=bandKPT,line_mode=False)
    BandDos = DOSPlotter_pretty(vb_energy_range=vb_energy_range,
                                  cb_energy_range=cb_energy_range)
    mplt = BandDos.get_plot(band_struct,cdos)
    if title==True:
        mplt.title(system + '\nBand gap: {:.0f} meV'.format(dosdata.eigenvalue_band_properties[0]*1000))
    mplt.savefig(imagename,dpi=600)
    print(system, ': ',dosdata.eigenvalue_band_properties)
