# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 10:04:41 2018

@author: phamtronglam2001@gmail.com
adopted and modified from 
http://home.ustc.edu.cn/~lipai/scripts/vasp_scripts/python_plot_dos_band.html
https://github.com/QijingZheng/pyband #for converting wavevector character to Greek symbol
Also modification of plotter module in pymatgen.electronic_structure were done to plot to axis handle
instead of creating completely new plot

Requirement:
    Band calculation: vasprun.xml, KPOINTS
    DOS calculation: vasprun.xml, POTCAR
"""
from plotBAND_DOS import plotBAND_DOS_2Dmaterial_style, plotBAND_DOS_pretty_style


"""
Silicene
"""
vb_energy_range=20
cb_energy_range=10

plotBAND_DOS_pretty_style(dosxml='../Silicene_unit_cell_literature/LDA/BAND/vasprun.xml',
                              dosPOTCAR='../Silicene_unit_cell_literature/LDA/BAND/POTCAR',
                              bandxml='../Silicene_unit_cell_literature/LDA/BAND/vasprun.xml',
                              bandKPT='../Silicene_unit_cell_literature/LDA/BAND/KPOINTS',
                              imagename='Silicene_unit_cell_literature_LDA.png',
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Silicene_unit_cell_literature LDA')


plotBAND_DOS_pretty_style(dosxml='../Silicene_unit_cell_literature/0-GGA/BAND/vasprun.xml',
                              dosPOTCAR='../Silicene_unit_cell_literature/0-GGA/BAND/POTCAR',
                              bandxml='../Silicene_unit_cell_literature/0-GGA/BAND/vasprun.xml',
                              bandKPT='../Silicene_unit_cell_literature/0-GGA/BAND/KPOINTS',
                              imagename='Silicene_unit_cell_literature_PBE.png',
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Silicene_unit_cell_literature PBE')

plotBAND_DOS_pretty_style(dosxml='../Silicene_unit_cell/0-GGA/DOS/vasprun.xml',
                              dosPOTCAR='../Silicene_unit_cell/0-GGA/DOS/POTCAR',
                              bandxml='../Silicene_unit_cell/0-GGA/BAND/vasprun.xml',
                              bandKPT='../Silicene_unit_cell/0-GGA/BAND/KPOINTS',
                              imagename='Silicene_unit_cell_PBE.png',
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Silicene_unit_cell PBE')
plotBAND_DOS_pretty_style(dosxml='../Silicene_unit_cell/1-revPBE/DOS/vasprun.xml',
                              dosPOTCAR='../Silicene_unit_cell/1-revPBE/DOS/POTCAR',
                              bandxml='../Silicene_unit_cell/1-revPBE/BAND/vasprun.xml',
                              bandKPT='../Silicene_unit_cell/1-revPBE/BAND/KPOINTS',
                              imagename='Silicene_unit_cell_1-revPBE.png',
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Silicene_unit_cell 1-revPBE')
plotBAND_DOS_pretty_style(dosxml='../Silicene_unit_cell/2-optPBE/DOS/vasprun.xml',
                              dosPOTCAR='../Silicene_unit_cell/2-optPBE/DOS/POTCAR',
                              bandxml='../Silicene_unit_cell/2-optPBE/BAND/vasprun.xml',
                              bandKPT='../Silicene_unit_cell/2-optPBE/BAND/KPOINTS',
                              imagename='Silicene_unit_cell_2-optPBE.png',
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Silicene_unit_cell 2-optPBE')
plotBAND_DOS_pretty_style(dosxml='../Silicene_unit_cell/3-dft-d2/DOS/vasprun.xml',
                              dosPOTCAR='../Silicene_unit_cell/3-dft-d2/DOS/POTCAR',
                              bandxml='../Silicene_unit_cell/3-dft-d2/BAND/vasprun.xml',
                              bandKPT='../Silicene_unit_cell/3-dft-d2/BAND/KPOINTS',
                              imagename='Silicene_unit_cell_3-dft-d2.png',
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Silicene_unit_cell 3-dft-d2')
plotBAND_DOS_pretty_style(dosxml='../Silicene_pristine_4x4/0-GGA/K12x12/DOS/vasprun.xml',
                              dosPOTCAR='POTCAR',
                              bandxml='../Silicene_pristine_4x4/0-GGA/K6x6/BAND/vasprun.xml',
                              bandKPT='../Silicene_pristine_4x4/0-GGA/K6x6/BAND/KPOINTS',
                              imagename='Silicene_pristine_4x4_GGA.png',
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Silicene_pristine_4x4 GGA')

plotBAND_DOS_pretty_style(dosxml='../Silicene_pristine_4x4/2-optPBE/K12x12/DOS/vasprun.xml',
                              dosPOTCAR='POTCAR',
                              bandxml='../Silicene_pristine_4x4/2-optPBE/K6x6/BAND/vasprun.xml',
                              bandKPT='../Silicene_pristine_4x4/2-optPBE/K6x6/BAND/KPOINTS',
                              imagename='Silicene_pristine_4x4_2-optPBE.png',
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Silicene_pristine_4x4 2-optPBE')

plotBAND_DOS_pretty_style(dosxml='../Silicene_pristine_4x4/dft-d3/K12x12/DOS/vasprun.xml',
                              dosPOTCAR='POTCAR',
                              bandxml='../Silicene_pristine_4x4/dft-d3/K6x6/BAND/vasprun.xml',
                              bandKPT='../Silicene_pristine_4x4/dft-d3/K6x6/BAND/KPOINTS',
                              imagename='Silicene_pristine_4x4_dft-d3.png',
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Silicene_pristine_4x4 dft-d3')