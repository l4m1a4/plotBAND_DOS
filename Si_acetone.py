# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 10:04:41 2018

@author: phamtronglam2001@gmail.com
adopted and modified from 
http://home.ustc.edu.cn/~lipai/scripts/vasp_scripts/python_plot_dos_band.html
https://github.com/QijingZheng/pyband #for converting wavevector character to Greek symbol
Also modification of plotter module in pymatgen.electronic_structure were done to plot to axis handle
instead of creating completely new plot

Requirement:
    Band calculation: vasprun.xml, KPOINTS
    DOS calculation: vasprun.xml, POTCAR
"""
from plotBAND_DOS import plotBAND_DOS_2Dmaterial_style, plotBAND_DOS_pretty_style

"""
Plot for Si_acetone
"""

dos_axis_scale = [1.05,0.7,1.3]
vb_energy_range=3
cb_energy_range=3

# 1-revPBE
plotBAND_DOS_2Dmaterial_style(dosxml='../Si_acetone/1-revPBE/K12x12/DOS/vasprun.xml',
                              dosPOTCAR='../Si_acetone/1-revPBE/K12x12/DOS/POTCAR',
                              bandxml='../Si_acetone/1-revPBE/K3x3/BAND/vasprun.xml',
                              bandKPT='../Si_acetone/1-revPBE/K3x3/BAND/KPOINTS',
                              imagename='Si_acetone_1-revPBE.png',
                              dos_axis_scale=dos_axis_scale,
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Si_acetone 1-revPBE')

# 2-optPBE
plotBAND_DOS_2Dmaterial_style(dosxml='../Si_acetone/2-optPBE/K12x12/DOS/vasprun.xml',
                              dosPOTCAR='POTCAR',
                              bandxml='../Si_acetone/2-optPBE/BAND/vasprun.xml',
                              bandKPT='../Si_acetone/2-optPBE/BAND/KPOINTS',
                              imagename='Si_acetone_2-optPBE.png',
                              dos_axis_scale=dos_axis_scale,
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Si_acetone 2-optPBE')
# 3-dft-d2
plotBAND_DOS_2Dmaterial_style(dosxml='../Si_acetone/3-dft-d2/K12x12/DOS/vasprun.xml',
                              dosPOTCAR='POTCAR',
                              bandxml='../Si_acetone/3-dft-d2/K3x3/BAND/vasprun.xml',
                              bandKPT='../Si_acetone/3-dft-d2/K3x3/BAND/KPOINTS',
                              imagename='Si_acetone_3-dft-d2.png',
                              dos_axis_scale=dos_axis_scale,
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Si_acetone 3-dft-d2')
# dft-d3
plotBAND_DOS_2Dmaterial_style(dosxml='../Si_acetone/dft-d3/K12x12/DOS/vasprun.xml',
                              dosPOTCAR='POTCAR',
                              bandxml='../Si_acetone/dft-d3/K6x6/BAND/vasprun.xml',
                              bandKPT='../Si_acetone/dft-d3/K6x6/BAND/KPOINTS',
                              imagename='Si_acetone_dft-d3.png',
                              dos_axis_scale=dos_axis_scale,
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Si_acetone DFT-D3')



