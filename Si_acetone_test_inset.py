# -*- coding: utf-8 -*-
"""
Created on Wed Oct 31 16:32:13 2018
"""
from plotBAND_DOS import plotBAND_DOS_2Dmaterial_style, plotBAND_DOS_pretty_style

"""
Plot for Si_acetone
"""

dos_axis_scale = [1.05,0.7,1.3]
vb_energy_range=6.0
cb_energy_range=4

plotBAND_DOS_2Dmaterial_style(dosxml='../Si_acetone/2-optPBE/K12x12/DOS/vasprun.xml',
                              dosPOTCAR='POTCAR',
                              bandxml='../Si_acetone/2-optPBE/BAND/vasprun.xml',
                              bandKPT='../Si_acetone/2-optPBE/BAND/KPOINTS',
                              imagename='Si_acetone_test_inset.png',
                              dos_axis_scale=dos_axis_scale,
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Si_acetone_test_inset')