# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 10:04:41 2018

@author: phamtronglam2001@gmail.com
adopted and modified from 
http://home.ustc.edu.cn/~lipai/scripts/vasp_scripts/python_plot_dos_band.html
https://github.com/QijingZheng/pyband #for converting wavevector character to Greek symbol
Also modification of plotter module in pymatgen.electronic_structure were done to plot to axis handle
instead of creating completely new plot

Requirement:
    Band calculation: vasprun.xml, KPOINTS
    DOS calculation: vasprun.xml, POTCAR
"""
from plotBAND_DOS import plotBAND_DOS_2Dmaterial_style, plotBAND_DOS_pretty_style

"""
Plot for Si_acetone
"""

dos_axis_scale = [1.05,0.3,1.3]
vb_energy_range=6.0
cb_energy_range=1.5

plotBAND_DOS_2Dmaterial_style(dosxml='vasprun_dos.xml',
                              dosPOTCAR='POTCAR',
                              bandxml='vasprun_band.xml',
                              bandKPT='KPOINTS_band',
                              imagename='example_2Dmaterial.png',
                              dos_axis_scale=dos_axis_scale,
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='example')
plotBAND_DOS_pretty_style(dosxml='vasprun_dos.xml',
                              dosPOTCAR='POTCAR',
                              bandxml='vasprun_band.xml',
                              bandKPT='KPOINTS_band',
                              imagename='example_pretty.png',
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='example')