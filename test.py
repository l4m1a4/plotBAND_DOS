# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 10:04:41 2018

@author: phamtronglam2001@gmail.com
adopted and modified from
http://home.ustc.edu.cn/~lipai/scripts/vasp_scripts/python_plot_dos_band.html
https://github.com/QijingZheng/pyband #for converting wavevector character to Greek symbol
Also modification of plotter module in pymatgen.electronic_structure were done to plot to axis handle
instead of creating completely new plot

Requirement:
    Band calculation: vasprun.xml, KPOINTS
    DOS calculation: vasprun.xml, POTCAR
"""
from plotBAND_DOS import plotBAND_DOS_2Dmaterial_style, plotBAND_DOS_pretty_style


"""
Silicene
"""
vb_energy_range=10
cb_energy_range=3


plotBAND_DOS_pretty_style(dosxml='../Silicene_unit_cell/0-GGA/DOS/vasprun.xml',
                              dosPOTCAR='../Silicene_unit_cell/0-GGA/DOS/POTCAR',
                              bandxml='../Silicene_unit_cell/0-GGA/BAND/vasprun.xml',
                              bandKPT='../Silicene_unit_cell/0-GGA/BAND/KPOINTS',
                              imagename='Silicene_unit_cell_PBE.eps',
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Silicene_unit_cell PBE')
