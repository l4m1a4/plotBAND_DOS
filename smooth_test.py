# -*- coding: utf-8 -*-
"""
Created on Sun Oct 14 13:10:16 2018

@author: phamtronglam2001@gmail.com

Plot band structure by smoothing procedure (this can affect Dirac cone).
reference:
https://plot.ly/ipython-notebooks/density-of-states/#plotting-density-of-states-with---plotly-and-pymatgen
"""

