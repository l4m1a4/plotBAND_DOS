# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 10:14:16 2018

@author: Administrator
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 10:04:41 2018

@author: phamtronglam2001@gmail.com
adopted and modified from 
http://home.ustc.edu.cn/~lipai/scripts/vasp_scripts/python_plot_dos_band.html
https://github.com/QijingZheng/pyband #for converting wavevector character to Greek symbol
Also modification of plotter module in pymatgen.electronic_structure were done to plot to axis handle
instead of creating completely new plot

Requirement:
    Band calculation: vasprun.xml, KPOINTS
    DOS calculation: vasprun.xml, POTCAR
"""
from plotBAND_DOS import plotBAND_DOS_2Dmaterial_style, plotBAND_DOS_pretty_style

"""
Plot for Si_trichloroethene
"""

dos_axis_scale = [0.7,0.5,2]
vb_energy_range=3
cb_energy_range=3


# 2-optPBE
plotBAND_DOS_2Dmaterial_style(dosxml='../Si_trichloroethene/2-optPBE/K12x12/DOS/vasprun.xml',
                              dosPOTCAR='POTCAR',
                              bandxml='../Si_trichloroethene/2-optPBE/K6x6/BAND/vasprun.xml',
                              bandKPT='../Si_trichloroethene/2-optPBE/K6x6/BAND/KPOINTS',
                              imagename='Si_trichloroethene_2-optPBE.png',
                              dos_axis_scale=[1,0.5,1.5],
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Si_trichloroethene 2-optPBE')

# dft-d3
plotBAND_DOS_2Dmaterial_style(dosxml='../Si_trichloroethene/dft-d3/K12x12/DOS/vasprun.xml',
                              dosPOTCAR='POTCAR',
                              bandxml='../Si_trichloroethene/dft-d3/K6x6/BAND/vasprun.xml',
                              bandKPT='../Si_trichloroethene/dft-d3/K6x6/BAND/KPOINTS',
                              imagename='Si_trichloroethene_dft-d3.png',
                              dos_axis_scale=[0.6,0.5,1.7],
                              vb_energy_range=vb_energy_range,
                              cb_energy_range=cb_energy_range,
                              title=True,
                              system='Si_trichloroethene DFT-D3')
